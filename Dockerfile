FROM golang:1.13

WORKDIR /go/src/http-endpoint

COPY endpoint.go .
RUN go build

EXPOSE 5000

ENTRYPOINT ["/go/src/http-endpoint/http-endpoint"]
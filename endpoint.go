package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"
	"net/http"
	"sync/atomic"
	"time"
)

var (
	_verbose   bool
	_listen    string
	_path      string
	_exitAfter uint64
	_slowReply time.Duration
)

func init() {
	flag.BoolVar(&_verbose, "verbose", false, "log requests and dump body of POSTs")
	flag.StringVar(&_listen, "listen", ":5000", "address to listen on")
	flag.StringVar(&_path, "path", "/submit", "request path")
	flag.Uint64Var(&_exitAfter, "exitAfter", 0, "Exit after this many POST messages have been received. 0 means no limit.")
	flag.DurationVar(&_slowReply, "slowReply", 0, "Delay results to requests by this time")
}

func main() {
	flag.Parse()

	var (
		numPost uint64
		notPost uint64
	)

	log.Println(`here we go, delaying replies by`, _slowReply)

	var timeStart atomic.Value

	http.HandleFunc(_path, func(w http.ResponseWriter, r *http.Request) {
		if _verbose {
			log.Println(r.Method, r.URL, "from", r.RemoteAddr)
		}

		if r.Method != "POST" {
			atomic.AddUint64(&notPost, 1)
			return
		}

		if atomic.LoadUint64(&numPost) == 0 {
			// This is the very first request
			timeStart.Store(time.Now())
		}

		atomic.AddUint64(&numPost, 1)

		time.Sleep(_slowReply)

		if !_verbose {
			return
		}

		b, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Println(`can't read body:`, err)
			return
		}
		var buf bytes.Buffer
		err = json.Indent(&buf, b, "", "\t")
		if err != nil {
			log.Printf(`can't pretty-print json body: %s, body: %q`, err, string(b))
			return
		}
		log.Printf("%s", buf.String())
	})

	go func() {
		log.Fatal(http.ListenAndServe(_listen, nil))
	}()

	var (
		handledBefore   uint64
		unhandledBefore uint64
	)

	for {
		time.Sleep(1 * time.Second)
		handled := atomic.LoadUint64(&numPost)
		unhandled := atomic.LoadUint64(&notPost)
		if handled != handledBefore || unhandled != unhandledBefore {
			log.Printf(`Handled %d POST requests so far, %d were not POST`, handled, unhandled)
		}
		handledBefore = handled
		unhandledBefore = unhandled
		if _exitAfter > 0 && handled >= _exitAfter {
			startTime := timeStart.Load().(time.Time)
			duration := time.Since(startTime)
			log.Println("Handled", handled, "messages in", duration)
			break
		}
	}
}
